# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import contracting
from . import employee
from . import salary_level
from . import party
from . import contract
from . import configuration
from . import office_specific
from . import position


def register():
    Pool.register(
        contracting.StaffUniform,
        contracting.UniformElement,
        contracting.EmployeeUniform,
        contracting.UniformLine,
        salary_level.SalaryLevel,
        employee.Employee,
        contracting.MedicalExam,
        employee.EmployeeCategory,
        contracting.EmployeeMedicalExam,
        party.Party,
        contract.Contract,
        contracting.ExamLine,
        employee.EmployeeCategoryMedicalExamDefault,
        employee.EmployeeCategoryUniformDefault,
        contracting.ContractingManpowerStart,
        contracting.ContractingGeneralStart,
        configuration.StaffConfiguration,
        office_specific.OfficeSpecific,
        employee.CreateMedicalExamStart,
        position.Position,
        position.PositionMedicalExamDefault,
        contract.ContractEvent,
        module='staff_contracting', type_='model')
    Pool.register(
        contracting.ContractingGeneralReport,
        contracting.ContractingManpowerReport,
        contracting.UniformReport,
        contracting.MedicalExamReport,
        contracting.FreeLiabilityReport,
        # contracting.employment_linkage_report,
        module='staff_contracting', type_='report')
    Pool.register(
        employee.CreateContractingAttributes,
        contracting.ContractingGeneral,
        contracting.ContractingManpower,
        contracting.CreateDefaultMedicalExams,
        contracting.CreateDefaultUniform,
        employee.CreateMedicalExam,
        module='staff_contracting', type_='wizard')
