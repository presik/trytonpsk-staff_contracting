# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime

from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

STATES = {'readonly': (Eval('state') != 'draft')}


class MedicalExam(ModelSQL, ModelView):
    "Empolyee Medical Exam"
    __name__ = 'staff.medical_exam'
    name = fields.Char('Name', required=True)
    description = fields.Char('Description')
    category = fields.Selection([
            ('laboratory', 'Laboratory'),
            ('paraclinic', 'Paraclinic'),
            ], 'Category', required=True)
    category_string = category.translated('category')

    @classmethod
    def __setup__(cls):
        super(MedicalExam, cls).__setup__()

    @staticmethod
    def default_category():
        return 'laboratory'


class EmployeeMedicalExam(Workflow, ModelSQL, ModelView):
    "Employee Medical Exams"
    __name__ = 'staff.employee.medical_exam'
    # _rec_name = 'employee'
    employee = fields.Many2One('company.employee', 'Employee',
            ondelete='CASCADE', required=True,
            states=STATES)
    exam_date = fields.Date('Date', states=STATES)
    exam_expiration = fields.Date('Expiration', states=STATES)
    laboratory = fields.Many2One('party.party', 'Laboratory',
            states=STATES)
    lines = fields.One2Many('staff.medical_exam.line', 'employee_medical_exam',
            'Lines', states=STATES)
    cause = fields.Selection([
            ('entry', 'Entry'),
            ('retirement', 'Retirement'),
            ('periodic', 'Periodic'),
            ('post_incapable', 'Post Incapable'),
            ('other', 'Other'),
        ], 'Cause', states=STATES)
    cause_string = cause.translated('cause')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),
            ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(EmployeeMedicalExam, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'cancel'),
                ('cancel', 'draft'),
                ('draft', 'done'),
                ('done', 'draft'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'done': {
                    'invisible': Eval('state') != 'draft',
                    },
                })

    @staticmethod
    def default_exam_date():
        return Pool().get('ir.date').today()

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_cause():
        return 'entry'

    def set_default_values(self):
        if not self.employee.category:
            return

        values = []
        for exam in self.employee.category.medical_exams:
            val = {
                'exam': exam.id,
                }
            values.append(val)
        if values:
            self.write([self], {'lines': [('create', values)]})

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('employee',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass


class ExamLine(ModelSQL, ModelView):
    "Medical Exam Line"
    __name__ = 'staff.medical_exam.line'
    employee_medical_exam = fields.Many2One('staff.employee.medical_exam',
            'Employee Medical Exam', required=True)
    exam = fields.Many2One('staff.medical_exam', 'Medical Exam',
            required=True)
    notes = fields.Char('Notes')


class UniformElement(ModelSQL, ModelView):
    "Staff Uniform Element"
    __name__ = 'staff.uniform.element'
    name = fields.Char('Name', required=False)


class EmployeeUniform(Workflow, ModelSQL, ModelView):
    "Employee Uniform"
    __name__ = 'staff.employee.uniform'
    # _rec_name = 'employee'
    employee = fields.Many2One('company.employee', 'Employee',
            required=True, states=STATES)
    date = fields.Date('Date', states=STATES)
    lines = fields.One2Many('staff.uniform.line', 'uniform',
            'Lines', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),
            ], 'State', readonly=True)

    @classmethod
    def __setup__(cls):
        super(EmployeeUniform, cls).__setup__()
        cls._transitions |= set((
                ('draft', 'cancel'),
                ('cancel', 'draft'),
                ('draft', 'done'),
                ('done', 'draft'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'done': {
                    'invisible': Eval('state') != 'draft',
                    },
                })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        return Pool().get('ir.date').today()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        pass

    def set_default_values(self):
        if not self.employee.category:
            return

        values = []
        for element in self.employee.category.uniforms:
            val = {
                'element': element.id,
                }
            values.append(val)
        if values:
            self.write([self], {'lines': [('create', values)]})

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('employee',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]


class UniformLine(ModelSQL, ModelView):
    "Staff Uniform Line"
    __name__ = 'staff.uniform.line'
    uniform = fields.Many2One('staff.employee.uniform',
            'Uniform')
    element = fields.Many2One('staff.uniform.element', 'Element',
            required=True)
    quantity = fields.Integer('Quantity', required=False)
    size = fields.Char('Size')
    description = fields.Char('Description')

    @classmethod
    def __setup__(cls):
        super(UniformLine, cls).__setup__()


class ContractingGeneralStart(ModelView):
    "Contracting General Start"
    __name__ = 'staff_contracting.general.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    department = fields.Many2One('company.department', 'Department')
    include_inactives = fields.Boolean('Include Inactives')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class ContractingGeneralReport(Report):
    "Contracting General Report"
    __name__ = 'staff_contracting.general_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        Employee = Pool().get('company.employee')
        dom = []
        dom2 = [('active', '=', False)]
        inactives = []
        if data['department']:
            dom.append(('department', '=', data['department']))
            dom2.append(('department', '=', data['department']))

        if data['include_inactives']:
            inactives = Employee.search(dom2, order=[('party.name', 'ASC')])

        actives = Employee.search(dom, order=[('party.name', 'ASC')])

        report_context['records'] = actives + inactives
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class ContractingGeneral(Wizard):
    "Contracting General"
    __name__ = 'staff_contracting.general'
    start = StateView('staff_contracting.general.start',
        'staff_contracting.contracting_general_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('staff_contracting.general_report')

    def do_print_(self, action):
        department_id = None
        if self.start.department:
            department_id = self.start.department.id
        data = {
            'ids': [],
            'company': self.start.company.id,
            'department': department_id,
            'include_inactives': self.start.include_inactives,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class ContractingManpowerReport(Report):
    "Contracting Manpower Report"
    __name__ = 'staff_contracting.manpower_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        Employee = Pool().get('company.employee')
        Payroll = Pool().get('staff.payroll')
        Payroll_line = Pool().get('staff.payroll.line')
        Period = Pool().get('staff.payroll.period')
        Department = Pool().get('company.department')
        dom = []
        clause = []
        start_date = None
        end_date = None
        period = None
        period_name = None
        department = None
        start_period = Period(data['start_period'])
        if data['end_period']:
            end_period = Period(data['end_period'])
            start_date = start_period.start
            end_date = end_period.end
            dom = [
                ('payroll.start', '>=', start_date),
                ('payroll.end', '<=', end_date),
            ]
            period_start = start_period.name
            period_end = end_period.name
        else:
            dom = [
                ('payroll.period.id', '=', start_period.id),
            ]
            period_start = start_period.name
            period_end = period_start.name
        if data['department']:
            dom.append(('payroll.employee.department', '=', data['department']))
            department = Department(data['department']).name

        dom.append(('wage_type.type_concept', 'in', ['salary', 'extras']))
        # dom.append(('payroll.state', '=', 'posted'))

        lines = Payroll_line.search(dom,
                order=[('payroll.employee.party.name', 'ASC')],
            )
        lines_dom = []
        party_ids = []
        payroll_ids = []
        hours = 0
        days = 0
        for line in lines:
            party_id = line.payroll.employee.party.id
            if party_id not in party_ids:
                hours = 0
            payroll_id = line.payroll.id
            hours = line.quantity + hours
            if party_id not in party_ids and payroll_id not in payroll_ids:
                days = line.payroll.worked_days
            if payroll_id not in payroll_ids and party_id in party_ids:
                days = line.payroll.worked_days + days
            type_document = ''
            if line.payroll.employee.party.type_document:
                if line.payroll.employee.party.type_document == '13':
                    type_document = 'Cédula de ciudadanía'
                elif line.payroll.employee.party.type_document == '11':
                    type_document = 'Registro Civil de Nacimiento'
                elif line.payroll.employee.party.type_document == '12':
                    type_document = 'Tarjeta de Identidad'
                elif line.payroll.employee.party.type_document == '21':
                    type_document = 'Tarjeta de Extranjeria'
                elif line.payroll.employee.party.type_document == '22':
                    type_document = 'Cedula de Extranjeria'
                elif line.payroll.employee.party.type_document == '31':
                    type_document = 'Nit'
                elif line.payroll.employee.party.type_document == '41':
                    type_document = 'Pasaporte'
                elif line.payroll.employee.party.type_document == '42':
                    type_document = 'Tipo de Documento Extranjero'
                elif line.payroll.employee.party.type_document == '43':
                    type_document = 'Sin identificacion del Exterior o para uso definido por la DIAN'
                else:
                    type_document = 'None'

            motherHead = 'No'
            if line.payroll.employee.party.motherHead:
                if line.payroll.employee.party.motherHead == True:
                    motherHead = 'Si'

            disability = 'No'
            if line.payroll.employee.party.disability:
                if line.payroll.employee.party.disability == True:
                    disability = 'Si'
            # sex = ''
            # if line.payroll.employee.party.sex:
            #     if line.payroll.employee.party.sex == 'male':
            #         sex = 'Masculino'
            #     else:
            #         sex = 'Femenino'

            required_formation = 'No'
            workInstalationClient = 'No'
            local_workforce = 'No'
            first_job = 'No'
            employment_benefits = ''
            clasification_activity = ''

            if line.payroll.employee.contract:
                if line.payroll.employee.contract.employment_benefits:
                    if line.payroll.employee.contract.employment_benefits == 'legal':
                        employment_benefits = 'Legales'
                    else:
                        employment_benefits = 'Convencionales'

                if line.payroll.employee.contract.clasification_activity:
                    if line.payroll.employee.contract.clasification_activity == 'operative':
                        clasification_activity = 'Operativa'
                    else:
                        clasification_activity = 'No_operativa'

                if line.payroll.employee.contract.workInstalationClient == True:
                    workInstalationClient = 'Si'

                if line.payroll.employee.contract.local_workforce == True:
                    local_workforce = 'Si'

                if line.payroll.employee.contract.required_formation == True:
                    required_formation = 'Si'

                if line.payroll.employee.contract.first_job == True and line.payroll.employee.party.age:
                    if int(line.payroll.employee.party.age) >= 18 and int(line.payroll.employee.party.age) <= 28:
                        first_job = 'Si'
            birthday = ''
            if line.payroll.employee.party.birthday:
                b = line.payroll.employee.party.birthday
                birthday = str(b.day) + '/' + str(b.month) + '/' + str(b.year)
            dv = ''
            nit = ''
            if line.payroll.employee.contract and line.payroll.employee.party:
                if line.payroll.employee.contract.nit_subcontracting:
                    dv = cls._get_check_digit(str(line.payroll.employee.contract.nit_subcontracting))
                    nit = str(line.payroll.employee.contract.nit_subcontracting) + '-' + dv

            value = {
                'line': line,
                'birthday': birthday,
                'hours': hours,
                'days': days,
                'employment_benefits': employment_benefits,
                'clasification_activity': clasification_activity,
                'first_job': first_job,
                'required_formation': required_formation,
                'local_workforce': local_workforce,
                'workInstalationClient': workInstalationClient,
                'disability': disability,
                'motherHead': motherHead,
                'type_document': type_document,
                'nit': nit,
            }

            lines_dom.append(value)
            if party_id in party_ids:
                index = len(lines_dom) - 2
                del (lines_dom[index])
            party_ids.append(party_id)
            payroll_ids.append(payroll_id)

        report_context['records'] = lines_dom
        report_context['company'] = user.company
        report_context['department'] = department
        report_context['user'] = user
        d = datetime.combine(start_period.start, datetime.min.time())
        report_context['month'] = d.month
        report_context['start'] = Period(data['start_period']).start
        report_context['end'] = period_end
        return report_context

    @classmethod
    def _get_check_digit(cls, id_number):
        PRIMOS = [71, 67, 59, 53, 47, 43, 41, 37, 29, 23, 19, 17, 13, 7, 3]
        if not id_number:
            return ' '
        id_number = id_number.replace(".", "")
        if not id_number.isdigit():
            return ' '
        c = 0
        p = len(PRIMOS) - 1
        for n in reversed(id_number):
            c += int(n) * PRIMOS[p]
            p -= 1

        dv = c % 11
        if dv > 1:
            dv = 11 - dv
        return str(dv)


class ContractingManpowerStart(ModelView):
    "Contracting Manpower Start"
    __name__ = 'staff_contracting.manpower.start'
    start_period = fields.Many2One('staff.payroll.period',
            'Start Period', required=True)
    end_period = fields.Many2One('staff.payroll.period', 'End Period',
            depends=['start_period'])
    company = fields.Many2One('company.company', 'Company', required=True)
    department = fields.Many2One('company.department', 'Department')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('start_period')
    def on_change_with_end_period(self, name=None):
        if self.start_period:
            return self.start_period.id


class ContractingManpower(Wizard):
    "Contracting Manpower"
    __name__ = 'staff_contracting.manpower'
    start = StateView('staff_contracting.manpower.start',
        'staff_contracting.contracting_manpower_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('staff_contracting.manpower_report')

    def do_print_(self, action):
        end_period_id = None
        department_id = None
        if self.start.end_period:
            end_period_id = self.start.end_period.id
        if self.start.department:
            department_id = self.start.department.id
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_period': self.start.start_period.id,
            'end_period': end_period_id,
            'department': department_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class UniformReport(Report):
    "Uniform Report"
    __name__ = 'staff_contracting.uniform_report'


class MedicalExamReport(Report):
    "Medical Exam Report"
    __name__ = 'staff_contracting.medical_exam_report'


class FreeLiabilityReport(Report):
    "Free Liability Report"
    __name__ = 'staff_contracting.free_liability_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class CreateDefaultMedicalExams(Wizard):
    "Create Default Medical Exams"
    __name__ = 'staff.contracting.create_default_medical_exam'
    start_state = 'create_exams'
    create_exams = StateTransition()

    def transition_create_exams(self):
        EmployeeMedExam = Pool().get('staff.employee.medical_exam')
        employee_med_exam_id = Transaction().context['active_id']
        employee_med_exam = EmployeeMedExam(employee_med_exam_id)
        employee_med_exam.set_default_values()
        return 'end'


class CreateDefaultUniform(Wizard):
    "Create Default Uniform"
    __name__ = 'staff.contracting.create_default_uniform'
    start_state = 'create_uniform'
    create_uniform = StateTransition()

    def transition_create_uniform(self):
        EmployeeUniform = Pool().get('staff.employee.uniform')
        employee_ids = Transaction().context['active_ids']
        employee_uniforms = EmployeeUniform.browse(employee_ids)
        for uniform in employee_uniforms:
            uniform.set_default_values()
        return 'end'


class StaffUniform(ModelSQL, ModelView):
    "Staff Uniform"
    __name__ = 'staff.uniform'
    element = fields.Selection([
            ('shirt', 'Shirt'),
            ('pants', 'Pants'),
            ('boots', 'Boots'),
            ('helmet', 'Helmet'),
            ('glass', 'Glass'),
            ('hearing_protectors', 'Hearing Protectors'),
            ('muffler', 'Muffler'),
            ('chest_guard', 'Chest Guard'),
            ('sleeve', 'Sleeve'),
            ('leggings', 'Leggings'),
            ('mask', 'Mask'),
            ('glove', 'Glove'),
            ('coverall', 'Coverall'),
            ('other', 'Other'),
            ('respirator', 'Respirator'),
            ('', ''),
            ], 'Element', required=True)
    element_string = element.translated('element')
    quantity = fields.Integer('Quantity')
    description = fields.Char('Description')
    size = fields.Char('Size')
    employee = fields.Many2One('company.employee', 'Employee')
    employee_category = fields.Many2One('staff.employee_category',
        'Employee Category')
    date = fields.Date('Date')


class EmploymentLinkageReport(Report):
    "Employment Linkage Report"
    __name__ = 'staff_contracting.employment_linkage_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context
